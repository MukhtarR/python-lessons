# # immutables = (int, str, tuple)
# # print(immutables[0:2])
#
# odd_set = set()
# even_set = set()
#
# for number in range(10):
#     if number % 2:
#         odd_set.add(number)
#     else:
#         even_set.add(number)
#
# # print(odd_set)
# # print(even_set)
#
# union_set = odd_set | even_set
# # union_set = odd_set.union(even_set)
#
# print(union_set)
# even_set.pop()
# even_set.remove(2)
#
# print(even_set)

# x1 = int(input())
# y1 = int(input())
# x2 = int(input())
# y2 = int(input())
# if abs(x1 - x2) <= 1 and abs(y1 - y2) <= 1:
#     print('YES')
# else:
#     print('NO')

# n = int(input())
# if 11 <= n <= 14:
#     print(n, 'korov')
# else:
#     temp = n % 10
#     if temp == 0 or (5 <= temp <= 9):
#         print(n, 'korov')
#     if temp == 1:
#         print(n, 'korova')
#     if 2 <= temp <= 4:
#         print(n, 'korovy')

# x = int(input())
# if x > 0:
#     print(1)
# elif x == 0:
#     print(0)
# else:
#     print(-1)

# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())

# import math
#
# a = int(input())
# b = int(input())
# c = int(input())
# if a + b <= c or a + c <= b or b + c <= a or a + b + c <= 0:
#     print('impossible')
# else:
#     if b < c:
#         t = b
#         b = c
#         c = t
#     if a < b:
#         t = a
#         a = b
#         b = t
#     if b < c:
#         t = b
#         b = c
#         c = t
#     if a < b:
#         t = a
#         a = b
#         b = t
#     if a ** 2 == b ** 2 + c ** 2:
#         print('rectangular')
#     else:
#         alpha = math.acos((b * b + c * c - a * a) / (2 * b * c))
#         if alpha < math.pi / 2:
#             print('acute')
#         else:
#             print('obtuse')


# a = int(input())
# b = int(input())
# c = int(input())
# if (a % 2 == 1 and b % 2 == 1 and c % 2 == 1)\
#         or (a % 2 == 0 and b % 2 == 0 and c % 2 == 0):
#     print("NO")
# else:
#     print("YES")

# a = int(input())
# b = int(input())
# c = int(input())
# if a > b:
#     a, b = b, a
# if b > c:
#     b, c = c, b
# if a > b:
#     a, b = b, a
# print(a, b, c)

# a = int(input())
# b = int(input())
# c = int(input())
# if a == b and b == c and a == c:
#     print(3)
# elif a == b or b == c or a == c:
#     print(2)
# else:
#     print(0)
#
# a = int(input())
# b = 1
# c = 0
# while b < a + 1:
#     c += b ** 2
#     b += 1
# print(c)

# print(input().count(' ') + 1)

# tag_list = ['python', 'course', 'coursera']
# print(', '.join(tag_list))

# one_element_tuple = (1, 2)
# guess_what = (1)
# one_element_tuple[3] = 9
# print(one_element_tuple[:])

# s = input()
# a = s[:s.find('h') + 1]
# b = s[s.find('h') + 1:s.rfind('h')]
# c = s[s.rfind('h'):]
# s = a + b.replace('h', 'H') + c
# print(s)

# s = input()
# for i in range(len(s)):
#     if i % 3 != 0:
#         print(s[i], end='')

# num = [1, 2, 3, 4, 5]


# # def funct(par):
# #     return str(par)
#
#
# def per(numq):
#     return list(map(lambda x: str(x), numq))
#
#
# print(per(range(10)))

# text = 'Love thy neighbor'
# # разделяем строку
# print(text.split())
# grocery = 'Milk, Chicken, Bread'
# # разделяем запятой
# print(grocery.split(', '))
# # разделяем двоеточием
# print(grocery.split('     '))
# print('------------------')
#
# grocery = 'Milk, Chicken, Bread, Butter'
# # maxsplit: 2
# print(grocery.split(', ', 2))
# # maxsplit: 1
# print(grocery.split(', ', 1))
# # maxsplit: 5
# print(grocery.split(', ', 15))
# # maxsplit: 0
# print(grocery.split(', ', 0))


# def rer(te):
#     tagList = []
#     for _ in te.split(','):
#         tagList.append(te.strip())
#
#     return tagList
# rer('hi, er')

# print(list(zip(
#   filter(bool, range(3)),
#   [x for x in range(3) if x]
# )))


# def max(a, b):
#     if a > b:
#         return a
#     print(6)
#     print(b)
#
#
# print(max(7, 4))

# Все параметры функции являются локальными переменными со значениями,
# которые были переданы в функцию. Параметры также можно изменять и это
# никак не повлияет на значения переменных в том месте, откуда была вызвана
# функция (если тип объектов-параметров был неизменяемым).
# def re(a, b):
#     print(a)
#     a = 1
#     print(a)
#
#
# f = 8
# re(f, 7)
# print(f)

# def IsPrime(n):
#     k = 2
#     p = 0
#     while pow(n, 0.5) >= k > 1:
#         if n % k == 0:
#             p += 1
#         k += 1
#     return p == 0
#
#
# n = int(input())
# if IsPrime(n):
#     print("YES")
# else:
#     print("NO")

# def tert():
#     b = int(input())
#     if b != 0:
#         tert()
#         print(b)
# tert()


# def IsPrime(n):
#     k = 2
#     p = 0
#     while pow(n, 0.5) >= k > 1:
#         if n % k == 0:
#             p += 1
#         k += 1
#     return p == 0
#
#
# n = int(input())
# if IsPrime(n):
#     print("YES")
# else:
#     print("NO")

# myTuple = (1, 'f', 3)
# s = 5, 6, 7
# t = 'text fg'
# print(myTuple.__add__(s))
# print(myTuple.__str__())
# print(tuple(t))
#
# for color in ('red', 'green', 'yellow'):
#     print(color, 'apple')
# for i in range(1, 100, 2):
#     print(i, "ss")

# a, b = int(input()), int(input())
# for _ in range(a, b + 1):
#     print(_, end=" ")


# a, b = int(input()), int(input())
# if a < b:
#     for _ in range(a, b + 1):
#         print(_, end=" ")
# else:
#     for _ in range(a, b - 1, -1):
#         print(_, end=" ")

# a = 1
# # b = a
# # a += 1
# # # a = 6
# # print(a, b)
# # c = [1, 2, 3]
# # d = c
# # print(d, c)
# # d[2] = "67"
# # print(c, d)
# # d = [1, 2, 3]
# # print(c, d)

# Заданы две клетки шахматной доски. Если они покрашены в один цвет,
# то выведите слово YES, а если в разные цвета – то NO.
# x1 = int(input())
# y1 = int(input())
# x2 = int(input())
# y2 = int(input())
# if (x1 + y1 + x2 + y2) % 2 == 0:
#     print('YES')
# else:
#     print('NO')

# Шоколадка имеет вид прямоугольника, разделенного на n×m долек. Шоколадку можно один раз разломить по прямой на две
# части. Определите, можно ли таким образом отломить от шоколадки часть, состоящую ровно из k долек.
#
# Формат ввода
#
# Программа получает на вход три числа: n, m, k.
#
# Формат вывода
#
#  Программа должна вывести одно из двух слов: YES или NO.
#
# n, m, k = int(input()), int(input()), int(input())
# if k < n * m and (k % n == 0 or k % m == 0):
#     print("YES")
# else:
#     print("NO")

# Даны координаты двух точек на плоскости, требуется определить, лежат ли они в одной координатной четверти или нет
# (все координаты отличны от нуля).
#
# Формат ввода
#
# Вводятся 4 числа: координаты первой точки (x1,y1) и координаты второй точки (x2,y2).
#
# Формат вывода
#
# Программа должна вывести слово YES, если точки находятся в одной координатной четверти, в противном случае вывести
# слово NO.

# x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())
# if x1 * x2 > 0 and y1 * y2 > 0:
#     print('YES')
# else:
#     print('NO')


# v1 = int(input())
# g1 = int(input())
# v2 = int(input())
# g2 = int(input())
# if g1 <= g2 and \
#         ((v2 % 2 == 1 and g2 % 2 == 1) or (v2 % 2 == 0 and g2 % 2 == 0)):
#     print("Yes")
# else:
#     print('No')

# A1 = int(input())  # Box 1
# B1 = int(input())
# C1 = int(input())
# A2 = int(input())  # Box 2
# B2 = int(input())
# C2 = int(input())
# if ((A1 == A2 and B1 == B2 and C1 == C2) or
#         (A1 == A2 and B1 == C2 and C1 == B2) or
#         (A1 == C2 and B1 == A2 and C1 == B2) or
#         (A1 == B2 and B1 == A2 and C1 == C2) or
#         (A1 == B2 and B1 == C2 and C1 == A2) or
#         (A1 == C2 and B1 == B2 and C1 == A2)):
#     print('Boxes are equal')
# elif ((A1 <= A2 and B1 <= B2 and C1 <= C2) or
#       (A1 <= A2 and B1 <= C2 and C1 <= B2) or
#       (A1 <= C2 and B1 <= A2 and C1 <= B2) or
#       (A1 <= B2 and B1 <= A2 and C1 <= C2) or
#       (A1 <= B2 and B1 <= C2 and C1 <= A2) or
#       (A1 <= C2 and B1 <= B2 and C1 <= A2)):
#     print('The first box is smaller than the second one')
# elif ((A1 >= A2 and B1 >= B2 and C1 >= C2) or
#       (A1 >= A2 and B1 >= C2 and C1 >= B2) or
#       (A1 >= C2 and B1 >= A2 and C1 >= B2) or
#       (A1 >= B2 and B1 >= A2 and C1 >= C2) or
#       (A1 >= B2 and B1 >= C2 and C1 >= A2) or
#       (A1 >= C2 and B1 >= B2 and C1 >= A2)):
#     print('The first box is larger than the second one')
# else:
#     print('Boxes are incomparable')

# По данному целому числу N распечатайте все квадраты натуральных чисел,не превосходящие N, в порядке возрастания.
# a = int(input())
# b = 1
# while b**2 < a:
#     print(b**2)
#     b += 1

# a = int(input())
# b = int(input())
# c = 1
# while a < b:
#     a += (a / 100) * 10
#     c += 1
# print(c)

# x = int(input())
# i = 2
# while x % i != 0:
#     i += 1
# print(i)

# x = int(input())
# maxx = x
# while x != 0:
#     x = int(input())
#     if maxx < x:
#         maxx = x
# print(maxx)

# x = int(input())
# maxx = x
# count = 1
# while x != 0:
#     x = int(input())
#     if maxx < x:
#         count = 1
#         maxx = x
#     elif maxx == x:
#         count += 1
# print(count)

# x = int(input())
# maxxx = x
# maxx = 0
# while x != 0:
#     x = int(input())
#     if maxxx < x:
#         maxx = maxxx
#         maxxx = x
#     elif maxxx > x:
#         if maxx < x:
#             maxx = x
# print(maxx)

# x = int(input())
# count = 1
# while x != 0:
#     x = int(input())
#     if x != 0:
#         count += 1
# print(count)

# num_even = -1
# element = -1
# while element != 0:
#     element = int(input())
#     if element % 2 == 0:
#         num_even += 1
# print(num_even)

# len = 0
# while int(input()) != 0:
#     len += 1
# print(len)


# n = int(input(яблоко))
# print('+___ ' * n)
# for i in range(n):
#     print('|', i + 1, ' /', sep='', end=' ')
# print()
# print('|__\\ ' * n)
# print('|    ' * n)

# a = int(input())
# b = int(input())
# c = int(input())
# p = (a + b + c) / 2
# s = (p * (p - a) * (p - b) * (p - c)) ** 0.5
# print(s)

# a = float(input())
# b = float(input())
# c = float(input())
# p = (a + b + c) / 2.  # полупериметр
# s = (p * (p - a) * (p - b) * (p - c))
# if s < 0:
#     s *= -1
# print('{:.6f}'.format(pow(s, 0.5)))

# По данному числу n вычислите сумму (1 / 1²)+(1 / 2²)+(1 / 3²)+...+(1 / n²).
# a = int(input())

# x = float(input())
# print(x % 1)

# x = float(input())
# print(int(x), round((x % 1) * 100))

# p = int(input())
# x = int(input())
# y = int(input())
# money_before = 100 * x + y
# money_after = int(money_before * (100 + p) / 100)
# print(money_after // 100, money_after % 100)

# По данному натуральному n≤9 выведите лесенку из n ступенек, i-я ступенька состоит из чисел от 1 до i без пробелов.
# p = int(input())
# for i in range(1, p + 1):
#     for j in range(1, i + 1):
#         print(j, sep='', end="")
#     print()


# p = int(input())
# cou = 0
# for i in range(p):
#     if int(input()) == 0:
#         cou += 1
# print(cou)


# for i in range(10, 100):
#     b = i % 10
#     o = i // 10
#     if (o * b * 2) == i:
#         print(i)

# a = '14 8 9'
# b = 'eer s'
# print(a)
# print(",".join(a))
# print(",".join(b))
# print(list(map(int, a.split())))
# print(*a)
# print(*b)
# Given a positive integer n. Print all n-digit odd positive integers in descending order.
# n = int(input())
# print(*([x for x in range(10 ** (n - 1), 10 ** n) if x % 2][::-1]))

# По данному натуральном n вычислите сумму 1²+2²+3²+...+n².
# For this natural n, calculate the sum of 1² + 2² + 3² + ... + n².
# n = int(input())
# count = 0
# for m in range(1, n + 1):
#     count += m ** 2
# print(count)


# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())
# e = int(input())
# if a <= d and b <= e or a <= e and b <= d:
#     print("YES")
# elif c <= d and b <= e or c <= e and b <= d:
#     print("YES")
# elif a <= d and c <= e or a <= e and c <= d:
#     print("YES")
# else:
#     print("NO")

# n = int(input())
# luy = n
# luym = 0
# for c in range(1, n):
#     n = int(input())
#     luy += c
#     luym  -= n
# print(luy + luym)
# print(luym)

# n = int(input())
# v = n
# b = 0
# for x in range(1, n):
#     n = int(input())
#     v += x
#     b -= n
# print(v + b)

# n = int(input())
# sum = 0
# for i in range(1, n + 1):
#     sum += i
# # можно доказать формулу:
# # sum == n * (n + 1) // 2
# # но мы посчитаем это значение циклом
# for i in range(n - 1):
#     sum -= int(input())
# print(sum)
# Даны два четырёхзначных числа A и B. Выведите все четырёхзначные числа на отрезке от A до B,
# запись которых является палиндромом.
# num1 = int(input())
# num2 = int(input())
#
# print(*[x for x in range(num1, num2 + 1) if
#         str(x) == str(x)[::-1] and len(str(x)) == 4], sep='\n')


# Выведите все элементы списка с четными индексами (то есть A[0], A[2], A[4], ...).
# Программа должна быть эффективной и не выполнять лишних действий!
# c = (input().split())
# for x in range(0, len(c), 2):
#     print(c[x], end=' ')

# a = input().split()
# for i in range(0, len(a), 2):
#     print(a[i])


# c = (list(map(int, input().split())))
# for x in c:
#     if x % 2 == 0:
#         print(x, end=' ')

# for x in range(len(c)):
#     print(c[x], "   :" , "x=", x, "c:", )

# ввод данных
# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())
# for x in range(1, 1001):
#     if a * x ** 3 + b * x ** 2 + c * x + d == 0:
#         print(x)

# print(len((tuple(x for x in list(map(int, input().split())) if x > 0))))

# Найдите наибольшее значение в списке и индекс последнего элемента, который имеет данное значение за один проход по
# списку, не модифицируя этот список и не используя дополнительного списка.

# l = list(map(int, input().split()))
# ind = 0
# maxc = 0
# z = -1
# for i, x in enumerate(l):
#     if x > maxc or x == z:
#         ind = i
#         maxc = x
#         z = maxc
# print(maxc, ind)
#
# a = list(map(int, input().split()))
# index = 0
# value = a[0]
# for ind, val in enumerate(a):
#     if val > value or val == value:
#         index, value = ind, val
# print(value, index)

# Отсортируйте данный массив, используя встроенную сортировку.
# Первая строка входных данных содержит количество элементов в массиве N, N ≤ 10⁵. Далее идет N целых чисел,
# не превосходящих по абсолютной величине 10⁹.
# n = int(input())
# m = [int(input()) for x in range(n)]
# print(*sorted(m))

# В обувном магазине продается обувь разного размера. Известно, что одну пару обуви можно надеть на другую, если она
# хотя бы на три размера больше. В магазин пришел покупатель.Требуется определить, какое наибольшее количество пар обуви
# сможет предложить ему продавец так, чтобы он смог надеть их все одновременно.
#
# r = int(input())
# l = []
# p = 0
# k = list(map(int, input().split()))
# for v in k:
#     if r <= v:
#         l.append(v)
#         r = v + 3
# print(len(l))
# k = int(input())
# N = [int(input("Введите число для добавления в список")) for i in range(k)]
# print(*sorted(N))
# По данному числу n вычислите сумму (1 / 1²)+(1 / 2²)+(1 / 3²)+...+(1 / n²).
# n = int(input())
# sumn = 0
# for x in range(1, n + 1):
#     sumn += 1 / x ** 2
# print('{:.5f}'.format(sumn))

#
# Сначала выведите третий символ этой строки.
# Во второй строке выведите предпоследний символ этой строки.
# В третьей строке выведите первые пять символов этой строки.
# В четвертой строке выведите всю строку, кроме последних двух символов.
# В пятой строке выведите все символы с четными индексами (считая, что индексация начинается с 0, поэтому символы
# выводятся начиная с первого).
# В шестой строке выведите все символы с нечетными индексами, то есть начиная со второго символа строки.
# В седьмой строке выведите все символы в обратном порядке.
# В восьмой строке выведите все символы строки через один в обратном порядке, начиная с последнего.
# В девятой строке выведите длину данной строки.
# a = input()
# print(a[2])
# print(a[-2])
# print(a[:5])
# print(a[:-2])
# print(a[::2])
# print(a[1::2])
# print(a[::-1])
# print(a[::-2])
# print(len(a))

# Дана строка. Если в этой строке буква f встречается только один раз, выведите её индекс. Если она встречается два и
# более раз, выведите индекс её первого и последнего появления. Если буква f в данной строке не встречается, ничего не
# выводите. При решении этой задачи нельзя использовать метод count и циклы
# string = input()
# pos = (string.find('f'))
# if pos != -1:
#     print(pos, end=' ')
# if string.find('f', pos + 1) != -1:
#     # print(string.find('f', -1))
#     print(string.rindex('f'))


# Дана строка, в которой буква h встречается минимум два раза.Удалите из этой строки первое и последнее вхождение
# буквы h,а также все символы, находящиеся между ними.
# string = input()
# pos = string.find('h')
# pos2 = string.rindex('h')
# fd = string[pos:pos2 + 1]
# print(string.replace(fd, ''))

# s = input()
# s = s[:s.find('h')] + s[s.rfind('h') + 1:]
# print(s)

# Дана строка, в которой буква h встречается как минимум два раза. Выведите измененную строку: повторите
# последовательность символов, заключенную между первым и последним появлением буквы h два раза (сами буквы h не входят
# в повторяемый фрагмент, т. е. их повторять не надо).
# string = input()
# pos1 = string.find('h')
# pos2 = string.rfind('h')
# fr = string[pos1 + 1:pos2]
# print(string.replace(fr, fr * 2))
# bf = 5
# print("hrke", bf, "!")
# py -3
# python
